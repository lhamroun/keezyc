NAME = keezy.bin

SRC = keezy.c
OBJ = keezy.o

FLAG = 
LIBRARY = arduino.a
INCLUDE_LIB = 
OBJ_LIB = object/SD/* object/SPI/* object/TMRpcm/*
INCLUDE = include/

CC = .bin/
ARG = -w -Os -g -flto -fuse-linker-plugin -Wl,--gc-sections -mmcu=atmega328p
OBJ_ARG = -O ihex -j .eeprom --set-section-flags=.eeprom=alloc,load --no-change-warnings --change-section-lma .eeprom=0
#OBJ_ARG = -O ihex -R .eeprom --set-section-flags=.eeprom=alloc,load --no-change-warnings --change-section-lma .eeprom=0

all: $(NAME)

$(NAME) : $(OBJ)
	$(CC) $(ARG) -o $(NAME) $(OBJ)

$(OBJ): $(SRC) include/keezy.h
	$(CC) -o $(OBJ) $(SRC) -I $(INCLUDE) -shared $(OBJ_LIB)

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all

.PHONY: all clean fclean re
