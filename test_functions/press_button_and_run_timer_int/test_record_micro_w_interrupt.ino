/* 
* this program trigger an ANALOG_COMP interrupt when we press the BUTTON
* and start a timer interrupt with SAMPLE_RATE period to catch MICROPHONE
* pin value.
*/

#define BUTTON 0x3 // analog pin 3
#define MICROPHONE 0x0
#define SAMPLE_RATE 11025

// determine ADC output compare mode (falling or rising)
uint8_t mode = 0b00000000;

/*
**  -----------------------------------------------------------------------
*/

void setup()
{
  Serial.begin(115200);
  Serial.println("begin");
  buttonListener(mode);
  Serial.println("initialization OK");
}

/*
**  -----------------------------------------------------------------------
*/

void  setTimer1ToSampleRate(void)
{
  // no prescaler, only internal clock //| CTC mode
  TCCR1B = (1 << CS10);// | (1 << WGM12);

  // clear TCCR1A
  TCCR1A = 0;

  // set tick limit (16MHz / 11025Hz) = 1451 ticks
  OCR1A = 1451;
}

/*
**  -----------------------------------------------------------------------
*/

void  buttonListener(uint8_t mode)
{
  //Serial.println("buttonListener");
  //normally this bit is set to clear register and hardware set when INT occur
  ACSR = 0;
  ACSR = (1 << ACI) | mode;
  //Serial.println("1.0");
  
  // set analog pins for negative input of comparator
  ADCSRA = (0 << ADEN);
  ADCSRB = (1 << ACME);
  //Serial.println("2");
  
  // use the pin of BUTTON for negative input of comparator
  ADMUX = BUTTON;
  //Serial.println("3");

  // put 0 on AIN0 (positive input) of the comparator
  DIDR1 = (1 << AIN0D);
  //Serial.println("4");
  
  // enable interrupt
  ACSR |= (1 << ACIE);
  //Serial.println("5");
}

/*
**  -----------------------------------------------------------------------
*/

int  ADCConversion(uint8_t pin)
{
  // save ADC register values
  uint8_t admux = ADMUX;
  uint8_t adcsra = ADCSRA;
  uint8_t adcsrb = ADCSRB;
  uint8_t low, high;
   
  //Serial.println("ADCConv");
  if (pin >= 14)
    pin -= 14;
  // AVCC with external capacitor at AREF pin
  ADMUX = (1 << REFS0) | (pin & 0x7);

  // enable ADC
  ADCSRA = (1 << ADEN);
  // set prescaler to 128 to have 125kHz
  ADCSRA |= (1 << ADPS0) | (1 << ADPS1) | (1 << ADPS2);

  //set free running mode
  ADCSRB = 0;

  // start conversion
  ADCSRA |= (1 << ADSC);

  // wait until end conversion
  while (ADCSRA & (1 << ADSC)) ;

  low = ADCL;
  high = ADCH;

  // restore ADC registers values
  ADMUX = admux;
  ADCSRA = adcsra;
  ADCSRB = adcsrb;

  return (high << 8) | low;
}

ISR(TIMER1_COMPA_vect)
{
  int ret;

  //Serial.println("ISR timer 1");
  ret = ADCConversion(MICROPHONE);
  cli();
  Serial.print("conversion result ");Serial.println(ret);
  sei();
}

/*
**  -----------------------------------------------------------------------
*/

ISR(BADISR_vect)
{
  Serial.println("ISR Error occurs");
}

/*
**  -----------------------------------------------------------------------
*/

ISR(ANALOG_COMP_vect)
{
  cli();
  Serial.println("ISR ADC");
  setTimer1ToSampleRate();

  Serial.print("ACSR reg ");Serial.println(ACSR, BIN);
  ACSR = (0 << ACIE);
  
  if (mode == (1 << ACIS1))
  {
    mode = (1 << ACIS1) | (1 << ACIS0);
    // enable interrupt
    TIMSK1 = (1 << OCIE1A);
  }
  else
  {
    mode = (1 << ACIS1);
    // disable interrupt
    TIMSK1 = 0;
  }
  
  buttonListener(mode); // reset ACIE (interrupt enable)
  Serial.print("ACSR reg ");Serial.println(ACSR, BIN);
  Serial.println("");
  sei();
}

/*
**  -----------------------------------------------------------------------
*/

void loop()
{
  Serial.println("loop");
  delay(5000);
}
