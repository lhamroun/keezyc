/**
 **	bool	WAVHeaderConcat(char *filename)
 **	This function take parameter filename which contain raw data of WAV file and
 **	the WAVHeaderConcat function calculate the size and create a WAV file with
 **	the good header
 **	returns : This function return true if no problem occurs
 **/

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define HEADER_SIZE 44

char	*setup_header(void)
{
	char	*file = malloc(HEADER_SIZE + 1);

	if (!file)
	{
		printf("malloc for header fail\n");
		return (0);
	}
	memset(file, 0, HEADER_SIZE + 1);
	// magic number RIFF big endian
	file[0] = 0x52;
	file[1] = 0x49;
	file[2] = 0x46;
	file[3] = 0x46;
	// file size (not know yet) little endian
	file[4] = 0;
	file[5] = 0;
	file[6] = 0;
	file[7] = 0;
	// WAVE flag big endian
	file[8] = 0x57;
	file[9] = 0x41;
	file[10] = 0x56;
	file[11] = 0x45;
	// fmt flag big endian
	file[12] = 0x66;
	file[13] = 0x6d;
	file[14] = 0x74;
	file[15] = 0x20;
	// data length little endian
	file[16] = 0x10;
	file[17] = 0x00;
	file[18] = 0x00;
	file[19] = 0x00;
	// PCM little endian
	file[20] = 0x01;
	file[21] = 0x00;
	// nb channels little endian
	file[22] = 0x01;
	file[23] = 0x00;
	// sample rate little endian (11025 Hz)
	file[24] = 0x11;
	file[25] = 0x2b;
	file[26] = 0x00;
	file[27] = 0x00;
	// (sample rate * bps * channel) / 8 little endian
	file[28] = 0x11;
	file[29] = 0x2b;
	file[30] = 0x00;
	file[31] = 0x00;
	// bps channel little endian
	file[32] = 0x01;
	file[33] = 0x00;
	// bps little endian
	file[34] = 0x08;
	file[35] = 0x00;
	// data text little endian
	file[36] = 0x64;
	file[37] = 0x61;
	file[38] = 0x74;
	file[39] = 0x61;
	// data size = file size - header size (not know yet) little endian
	file[40] = 0;
	file[41] = 0;
	file[42] = 0;
	file[43] = 0;
	// final '\0'
	file[44] = 0;
	return (file);
}

void	WAVHeaderConcat(int fd)
{
	int		i = 0;
	int		newFile = -1;
	char	*header = NULL;
	char	buf[128];
	int		count = 0;;

	newFile = open("newFile.wav", O_CREAT | O_RDWR);
	if (newFile == -1)
	{
		printf("new file can't be created\n");
		exit(0);;
	}
	header = setup_header();
	write(newFile, header, 44);
	while (1)
	{
		count = read(fd, buf, 127);
		if (count == -1)
		{
			printf("erreur de lecture\n");
			exit(0);
		}
		buf[count] = '\0';
		write(newFile, buf, count);
		if (!count)
		{
			printf("fin du fichier\n");
			break ;
		}
		++i;
	}
	unsigned int fileSize = count + i * 127 + 44;
	printf("fileSize %x\n", fileSize);
	lseek(newFile, 4, 0);
	unsigned char	a = fileSize & 0x000000ff;
	unsigned char	b = (fileSize & 0x0000ff00) >> 8;
	unsigned char	c = (fileSize & 0x00ff0000) >> 16;
	unsigned char	d = (fileSize & 0xff000000) >> 24;
	printf("a %hhx\n", a);
	printf("b %hhx\n", b);
	printf("c %hhx\n", c);
	printf("d %hhx\n", d);
	write(newFile, &a, 1);
	write(newFile, &b, 1);
	write(newFile, &c, 1);
	write(newFile, &d, 1);
	lseek(newFile, 40, 0);
	fileSize = count + i * 127;
	a = fileSize & 0x000000ff;
	b = (fileSize & 0x0000ff00) >> 8;
	c = (fileSize & 0x00ff0000) >> 16;
	d = (fileSize & 0xff000000) >> 24;
	write(newFile, &a, 1);
	write(newFile, &b, 1);
	write(newFile, &c, 1);
	write(newFile, &d, 1);
	close(newFile);
}

int		main(int ac, char **av)
{
	int fd = -1;

	if (ac != 2)
	{
		printf("usage: ./WAVconcat [filename]\n");
		return (0);
	}
	fd = open(av[1], O_RDONLY);
	if (fd == -1)
	{
		printf("file %s not found\n", av[1]);
		return (0);
	}
	WAVHeaderConcat(fd);
	close(fd);
	return (0);
}
//	printf("\n");
