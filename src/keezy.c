#include <SD.h>
#include <SPI.h>
#include <TMRpcm.h>

#define SDPIN 4
#define MICRO_PIN A0
#define MICRO_BUT 7 // use to activate micro (green button)
#define LED_PIN 8 // blue led pin
#define MUX_PIN A3 // pin use for multiplexer
#define S0 3 // input multiplexer 1
#define S1 5 // input multiplexer 2
#define S2 6 // input multiplexer 3
#define MICRO_FQ 11025 // WAV sample frequency
#define MAX_TRY 5 // try to start SD
#define HEADER_SIZE 44UL // WAV file header len
#define RESET_COUNT 5 // blink RESET_COUNT times to reset output music
#define AUDIO_DATA "AUDIO_DATA" // temporary file name for recording microphone raw data
#define RECORDED_FILE "TMP_MICRO.WAV" // temporary file with microphone datas
#define INIT_SOUND_NAME "INIT.WAV" // file play on initialisation
#define WRONG_SOUND_NAME "WRONG.WAV" // file play when an error occur during recording
#define RESET_SOUND_NAME "RESET.WAV" // file play when we reset music

// use to status recording
enum          e_recordFlag
{
  NO_RECORDING = 0,
  TO_RECORD = 1,
  ON_RECORDING = 2,
  STOP_RECORDING = 3
};

File          recordedFile;
File          dataFile;
TMRpcm        tmrpcm;
bool          sdFlag = true;
int           recorderFlag = 0;
bool          microButton = LOW;
byte          playButton = 0;
int           mux[8] = {0};
int           resetCount = 0;
bool          resetCountHelp = false;
char          *recordName[] = {"1.WAV", "2.WAV", "3.WAV", "4.WAV", "5.WAV", "6.WAV", "7.WAV", "8.WAV"};
char          *defaultName[] = {"DEFAULT1.WAV", "DEFAULT2.WAV", "DEFAULT3.WAV", "DEFAULT4.WAV", "DEFAULT5.WAV", "DEFAULT6.WAV", "DEFAULT7.WAV", "DEFAULT8.WAV"};
char          *currentName[] = {"DEFAULT1.WAV", "DEFAULT2.WAV", "DEFAULT3.WAV", "DEFAULT4.WAV", "DEFAULT5.WAV", "DEFAULT6.WAV", "DEFAULT7.WAV", "DEFAULT8.WAV"};

/*********************************************************************/
/**   setup function                                                **/
/*********************************************************************/

void setup()
{
  Serial.begin(115200);
  Serial.println("Init SD card :");
  int sdTry = 0;
  while (sdTry < MAX_TRY)
  {
    delay(300);
    if (SD.begin(SDPIN))
      break ;
    Serial.print("SD.begin fail ");Serial.print(sdTry + 1);
    Serial.print("/");Serial.println(MAX_TRY);
    sdTry++;
  }
  if (sdTry == MAX_TRY)
  {
    Serial.println("SD init fail");
    while (1);
  }
  else
  {
    Serial.println("SD init OK");
    sdFlag = true;
    Serial.println("Init I/O pins");
    pinMode(MICRO_PIN, INPUT);
    pinMode(MUX_PIN, INPUT);
    pinMode(MICRO_BUT, INPUT);
    pinMode(LED_PIN, OUTPUT);
    pinMode(S0, OUTPUT);
    pinMode(S1, OUTPUT);
    pinMode(S2, OUTPUT);
    Serial.println("Init OK");
    tmrpcm.play(INIT_SOUND_NAME);
  }
}

/*********************************************************************/
/**   set WAV header                                                **/
/*********************************************************************/

bool  setupNewWAVFile(unsigned long dataSize)
{
  byte  a;
  byte  b;
  byte  c;
  byte  d;
  
  recordedFile = SD.open(RECORDED_FILE, FILE_WRITE);
  if (!recordedFile)
  {
    Serial.println("error creating header of micro.wav");
    return false;
  }
  Serial.println("start to store WAV header");

  dataSize += HEADER_SIZE;
  a = (dataSize & 0xFF000000) >> 24;
  b = (dataSize & 0x00FF0000) >> 16;
  c = (dataSize & 0x0000FF00) >> 8;
  d = dataSize & 0x000000FF;
  
  // magic number RIFF big endian
  recordedFile.print(0x52);
  recordedFile.print(0x49);
  recordedFile.print(0x46);
  recordedFile.print(0x46);
  // file size (not know yet) little endian
  recordedFile.print(d);
  recordedFile.print(c);
  recordedFile.print(b);
  recordedFile.print(a);
  // WAVE flag big endian
  recordedFile.print(0x57);
  recordedFile.print(0x41);
  recordedFile.print(0x56);
  recordedFile.print(0x45);
  // fmt flag big endian
  recordedFile.print(0x66);
  recordedFile.print(0x6d);
  recordedFile.print(0x74);
  recordedFile.print(0x20);
  // data length little endian
  recordedFile.print(0x10);
  recordedFile.print(0x00);
  recordedFile.print(0x00);
  recordedFile.print(0x00);
  // PCM little endian
  recordedFile.print(0x01);
  recordedFile.print(0x00);
  // nb channels little endian
  recordedFile.print(0x01);
  recordedFile.print(0x00);
  // sample rate (11025) little endian
  recordedFile.print(0x11);
  recordedFile.print(0x2b);
  recordedFile.print(0x00);
  recordedFile.print(0x00);
  // (sample rate * bps * channel) / 8 little endian
  recordedFile.print(0x11);
  recordedFile.print(0x2b);
  recordedFile.print(0x00);
  recordedFile.print(0x00);
  // bps channel little endian
  recordedFile.print(0x01);
  recordedFile.print(0x00);
  // bps little endian
  recordedFile.print(0x08);
  recordedFile.print(0x00);
  // data text little endian
  recordedFile.print(0x64);
  recordedFile.print(0x61);
  recordedFile.print(0x74);
  recordedFile.print(0x61);
  // data size = file size - header size (not know yet) little endian
  dataSize -= HEADER_SIZE;
  a = (dataSize & 0xFF000000) >> 24;
  b = (dataSize & 0x00FF0000) >> 16;
  c = (dataSize & 0x0000FF00) >> 8;
  d = dataSize & 0x000000FF;
  recordedFile.print(d);
  recordedFile.print(c);
  recordedFile.print(b);
  recordedFile.print(a);

  recordedFile.close();
  //delay(500);
  Serial.println("finish to copy WAV header");
  return true;
}

/********************************************************************************/
/**  Fill audio data on micro.wav                                              **/
/********************************************************************************/

bool  fillAudioDataOnFile(void)
{
  byte          buff = 0;
  int           ptr1 = 0;
  int           ptr2 = HEADER_SIZE;

// start copying raw audio data on micro.wav
  Serial.println("copy on micro.wav start");
  dataFile = SD.open(AUDIO_DATA, FILE_READ);
  if (!dataFile)
  {
    Serial.println("error2 audio_data");
    return false;
  }
  while (dataFile.available())
  {
    ptr1 = dataFile.peek() + 1;
    buff = dataFile.read();
    dataFile.close();
    
    recordedFile = SD.open(RECORDED_FILE, FILE_WRITE);
    if (!recordedFile)
    {
      Serial.println("error3 micro.wav");
      return false;
    }
    recordedFile.seek(ptr2);
    ptr2 = recordedFile.peek() + 1;
    recordedFile.print(buff);
    recordedFile.close();
  
    dataFile = SD.open(AUDIO_DATA, FILE_READ);
    if (!dataFile)
    {
      Serial.println("error3 audio_data");
      return false;
    }
    dataFile.seek(ptr1);
  }
  dataFile.close();
  Serial.println("end of copying audio_data");
  return true;
}

/*********************************************************************/
/**   count audio data size                                         **/
/*********************************************************************/

unsigned long countDataSize(void)
{
  unsigned long dataSize = 0;

  dataFile = SD.open(AUDIO_DATA, FILE_READ);
  if (!dataFile)
  {
    Serial.println("fail to open micro_data");
    return (-1);
  }
  dataSize = dataFile.size();
  Serial.print("dataSize ");Serial.println(dataSize);
  dataFile.close();
  return (dataSize);
}

/*********************************************************************/
/**   File creator                                                  **/
/*********************************************************************/

void  createFile(void)
{
  unsigned long dataSize = 0;

  dataSize = countDataSize();
  if (dataSize == -1)
  {
    Serial.println("error during countDataSize()");
    tmrpcm.play(WRONG_SOUND_NAME);
    return ;
  }
  if (!setupNewWAVFile(dataSize))
  {
    Serial.println("error during setupNewWavFile()");
    tmrpcm.play(WRONG_SOUND_NAME);
    return ;
  }
  if (!fillAudioDataOnFile())
  {
    Serial.println("error during fillAudioDataOnFile()");
    tmrpcm.play(WRONG_SOUND_NAME);
    return ;
  }
  Serial.println("stop recording, everything is OK.");
}

/*********************************************************************/
/**   recording manager                                             **/
/*********************************************************************/

void  tryToRecord(void)
{ 
  if (recorderFlag == TO_RECORD)
  {
    Serial.println("start recording !!");
    digitalWrite(LED_PIN, HIGH);
    recorderFlag = ON_RECORDING;
    currentName[playButton] = recordName[playButton];
    tmrpcm.startRecording(currentName[playButton], MICRO_FQ, MICRO_PIN);
  }
  else if (recorderFlag == STOP_RECORDING)
  {
    tmrpcm.stopRecording(currentName[playButton]);
    recorderFlag = NO_RECORDING;
    digitalWrite(LED_PIN, LOW);
    if (!SD.exists(AUDIO_DATA))
    {
      Serial.println("file micro_data don't exist");
      tmrpcm.play(WRONG_SOUND_NAME);
      return ;
    }
    createFile();
  }
}

/*********************************************************************/
/**   find which button use                                         **/
/*********************************************************************/

byte  findLastButton(void)
{
  for (int i = 0; i < 8; i++)
  {
    if (mux[i] > 400)
      return (i);
  }
  return (0);
}

/*********************************************************************/
/**   read values of 8 pins from multiplexer                        **/
/*********************************************************************/

void  readMux(void)
{
  for (int i = 0; i < 8; i++)
  {
    digitalWrite(S0, i & 0b00000001);
    digitalWrite(S1, i & 0b00000010);
    digitalWrite(S2, i & 0b00000100);
    mux[i] = analogRead(MUX_PIN);
  }
}

/*********************************************************************/
/**   button event                                                  **/
/*********************************************************************/

void  waitButtonEvent(void)
{
  readMux();
  playButton = findLastButton();
  microButton = digitalRead(MICRO_BUT);
  if (microButton == HIGH && recorderFlag != ON_RECORDING && playButton)
    recorderFlag = TO_RECORD;
  else if (microButton == LOW && recorderFlag == ON_RECORDING)
    recorderFlag = STOP_RECORDING;
  else if (microButton == HIGH && resetCountHelp == false)
  {
    resetCountHelp = true;
    resetCount++;
  }
  else if (microButton == LOW)
    resetCountHelp = false;
  if (playButton != 0 && microButton == LOW)
    tmrpcm.play(currentName[playButton]);
    
}

/********************************************************************************/
/**  main functions                                                            **/
/********************************************************************************/

void  resetSound(void)
{
  for (int i = 0; i < 8; i++)
  {
    memcpy(currentName[i], defaultName[i], 12);
  }
  resetCount = 0;
  resetCountHelp = false;
  tmrpcm.play(RESET_SOUND_NAME);
}

/********************************************************************************/
/**  main functions                                                            **/
/********************************************************************************/

void loop()
{
  if (sdFlag)
  {
    if (resetCount == RESET_COUNT)
      resetSound();
    waitButtonEvent();
    tryToRecord();
  }
}
//Serial.print("but ");Serial.println(playButton);
//Serial.println("---------------------");
